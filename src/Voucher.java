import java.util.*;

public class Voucher {

    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);
        System.out.println("Masukkan point : ");
        int input = myObj.nextInt();


        int[] voucher = {10, 25, 50, 100};
        int[] point = {100, 200, 400, 800};

        //order voucher desc
        int voucherTemp = 0;
        for(int i= 0; i<voucher.length; i++) {
            for(int j=i+1; j<voucher.length; j++) {
                if(voucher[i]<voucher[j]) {
                    voucherTemp = voucher[i];
                    voucher[i] = voucher[j];
                    voucher[j] = voucherTemp;
                }
            }
        }

        //order point desc
        int pointTemp = 0;
        for(int i= 0; i<point.length; i++) {
            for(int j=i+1; j<point.length; j++) {
                if(point[i]<point[j]) {
                    pointTemp = point[i];
                    point[i] = point[j];
                    point[j] = pointTemp;
                }
            }
        }

        int maxVoucher = 0;
        int sisaPoint = 0;

        for (int i=0; i<voucher.length; i++) {
            if(input > point[i]) {
                maxVoucher = voucher[i];
                sisaPoint = input - point[i];
                break;
            }
        }

        List<Integer> rendemAll = new ArrayList<Integer>();
        int inputTemp = input;

        while (inputTemp > point[3]) {
            while (input >= point[0]) {
                inputTemp -= point[0];
                rendemAll.add(100);
                if(inputTemp < point[0]) {
                    break;
                }
            }
            while (inputTemp >= point[1]) {
                inputTemp -= point[1];
                rendemAll.add(50);
            }
            while (inputTemp >= point[2]) {
                inputTemp -= point[2];
                rendemAll.add(25);
            }
            while (inputTemp >= point[3]) {
                inputTemp -= point[3];
                rendemAll.add(10);
            }
        }
        rendemAll.add(inputTemp);

        System.out.println("Voucher Terbesar : " + maxVoucher +" rb");
        System.out.println("---------------------------------------");
        System.out.println("Sisa Point : " + sisaPoint + " point");
        System.out.println("---------------------------------------");
        System.out.println("Redem All Point : ");
        for(int i=0; i<rendemAll.size()-1; i++) {
            System.out.println(i+1+"." + " "+rendemAll.get(i)+ " "+"rb");
        }
        System.out.println(rendemAll.size()+"."+" "+"Sisa point : "+" "+rendemAll.get(rendemAll.size()-1)+" "+"point");

    }
}
